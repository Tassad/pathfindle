use std::borrow::Cow;

use proc_macro2::TokenStream;
use quote::{quote, ToTokens, TokenStreamExt};
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct PFFeat<'a> {
    pub name: Cow<'a, str>,
    pub tags: Option<Cow<'a, str>>,
    pub description: Cow<'a, str>,
    pub prerequisites: Option<Cow<'a, str>>,
    pub benefit: Cow<'a, str>,
    pub normal: Option<Cow<'a, str>>,
    pub special: Option<Cow<'a, str>>,
    pub source: Cow<'a, str>,
    pub note: Option<Cow<'a, str>>,
    pub goal: Option<Cow<'a, str>>,
    pub completion_benefit: Option<Cow<'a, str>>,
    pub suggested_traits: Option<Cow<'a, str>>,
}

impl ToTokens for PFFeat<'_> {
    fn to_tokens(&self, tokens: &mut TokenStream) {
        let name = (&self.name).tokenize();
        let tags = (&self.tags).tokenize();
        let description = (&self.description).tokenize();
        let prerequisites = (&self.prerequisites).tokenize();
        let benefit = (&self.benefit).tokenize();
        let normal = (&self.normal).tokenize();
        let special = (&self.special).tokenize();
        let source = (&self.source).tokenize();
        let note = (&self.note).tokenize();
        let goal = (&self.goal).tokenize();
        let completion_benefit = (&self.completion_benefit).tokenize();
        let suggested_traits = (&self.suggested_traits).tokenize();

        tokens.append_all(quote!(
            PFFeat {
                name: #name,
                tags: #tags,
                description: #description,
                prerequisites: #prerequisites,
                benefit: #benefit,
                normal: #normal,
                special: #special,
                source: #source,
                note: #note,
                goal: #goal,
                completion_benefit: #completion_benefit,
                suggested_traits: #suggested_traits,
            }
        ));
    }
}

trait Tokenize {
    fn tokenize(&self) -> TokenStream;
}

impl Tokenize for &Cow<'_, str> {
    fn tokenize(&self) -> TokenStream {
        quote!(::std::borrow::Cow::Borrowed(#self))
    }
}

impl Tokenize for &Option<Cow<'_, str>> {
    fn tokenize(&self) -> TokenStream {
        if let Some(s) = self {
            quote!(::std::option::Option::Some(::std::borrow::Cow::Borrowed(#s)))
        } else {
            quote!(::std::option::Option::None)
        }
    }
}
