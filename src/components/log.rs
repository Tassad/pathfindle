use indexmap::IndexMap;
use yew::prelude::*;

#[derive(PartialEq, Properties)]
pub struct LogProps {
    pub guess_log: UseStateHandle<IndexMap<AttrValue, AttrValue>>,
    pub last_guess: UseStateHandle<Option<AttrValue>>,
}

#[function_component]
pub fn Log(props: &LogProps) -> Html {
    let LogProps {
        guess_log,
        last_guess,
    } = props;

    let last_guess = (**last_guess).as_ref();

    html! {
        <>
            <ol reversed={true} class={classes!("p-2", "min-h-min")}>
                if let Some(guess) = last_guess {
                    if let Some(count) = guess_log.get(guess) {
                        <li key={guess.to_string() + "top"} class={classes!("flex", "justify-between", "bg-gray-700")}>
                            <span class={classes!("overflow-x-auto")}>{ format!("{guess} ") }</span>
                            <span class={classes!("text-right", "pl-2")}>{ format!("{count}") }</span>
                        </li>
                    }
                }
                {
                    guess_log.iter().rev().map(|(guess, count)| html! {
                        <li key={guess.to_string()} class={classes!("flex", "justify-between")}>
                            <span class={classes!("overflow-x-auto")}>{ format!("{guess} ") }</span>
                            <span class={classes!("text-right", "pl-2")}>{ format!("{count}") }</span>
                        </li>
                    }).collect::<Html>()
                }
            </ol>
        </>
    }
}
