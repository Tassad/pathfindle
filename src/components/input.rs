use wasm_bindgen::JsCast;
use web_sys::HtmlInputElement;
use yew::prelude::*;

#[derive(PartialEq, Properties)]
pub struct InputProps {
    pub on_guess: Callback<AttrValue>,
}

#[function_component]
pub fn Input(props: &InputProps) -> Html {
    let InputProps { on_guess } = props;

    let input = use_state(String::default);

    let oninput = {
        let input = input.clone();

        Callback::from(move |e: InputEvent| {
            e.prevent_default();

            let new_input = e
                .target()
                .and_then(|t| t.dyn_into::<HtmlInputElement>().ok());

            if let Some(new_input) = new_input {
                input.set(new_input.value());
            }
        })
    };

    let onsubmit = {
        let on_guess = on_guess.clone();
        let input = input.clone();

        Callback::from(move |e: SubmitEvent| {
            e.prevent_default();

            if !input.is_empty() {
                on_guess.emit(input.to_lowercase().into());
            }

            input.set("".to_string());
        })
    };

    html! {
        <>
            <form {onsubmit} autocomplete="off" class={classes!("flex", "justify-between", "space-x-2", "px-1", "items-center")}>
                <p class={classes!("w-8", "min-w-fit", "text-right")}>{ format!(" {}", input.chars().count()) }</p>
                <label class={classes!("hidden")} for="guess">{"Type your guess: "}</label>
                <input {oninput} value={input.to_string()} class={classes!("border", "border-black", "text-black", "flex-1")} type="text" id="guess" />
                <button type="submit">{ "➡️" }</button>
            </form>
        </>
    }
}
