use yew::prelude::*;

#[function_component]
pub fn Footer() -> Html {
    html! {
        <footer class={classes!("min-h-0", "p-1", "bg-gray-700", "text-xs")}>
            <a href="https://gitlab.com/Tassad/pathfindle" class={classes!("underline", "hover:no-underline", "hover:text-blue-300", "text-blue-400")}>
                { "Source code" }
            </a>
            <p>
                { "Pathfindle uses trademarks and/or copyrights owned by Paizo Inc., used under Paizo's Community Use Policy (" }
                <a href="https://paizo.com/communityuse" class={classes!("underline", "hover:no-underline", "hover:text-blue-300", "text-blue-400")}>{ "paizo.com/communityuse" }</a>
                { "). We are expressly prohibited from charging you to use or access this content. Pathfindle is not published, endorsed, or specifically approved by Paizo. For more information about Paizo Inc. and Paizo products, visit " }
                <a href="https://paizo.com" class={classes!("underline", "hover:no-underline", "hover:text-blue-300", "text-blue-400")}>{ "paizo.com" }</a>
                { "." }
            </p>
        </footer>
    }
}
