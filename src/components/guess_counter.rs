use yew::prelude::*;

#[derive(PartialEq, Properties)]
pub struct GuessCounterProps {
    pub guess_counter: UseStateHandle<u32>,
}

#[function_component]
pub fn GuessCounter(props: &GuessCounterProps) -> Html {
    let GuessCounterProps { guess_counter } = props;

    html! {
        <>
            <h2 class={classes!("lg:text-2xl", "p-2")}>{ "Guesses: " }{ guess_counter.to_string() }</h2>
        </>
    }
}
