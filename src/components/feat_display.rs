use yew::prelude::*;

use crate::redacted::RedactedFeat;

#[derive(PartialEq, Properties)]
pub struct FeatDisplayProps {
    pub feat: UseStateHandle<RedactedFeat>,
}

#[function_component]
pub fn FeatDisplay(props: &FeatDisplayProps) -> Html {
    let FeatDisplayProps { feat } = props;

    let hidden_handle = use_state(|| true);

    let onclick = {
        let hidden_handle = hidden_handle.clone();

        Callback::from(move |_| {
            hidden_handle.set(!*hidden_handle);
        })
    };

    html! {
        <>
            <h1 class={classes!("font-bold", "text-5xl", "py-1")}>
                { feat.answer.to_revealables(onclick.clone(), hidden_handle.clone()) }
                if !feat.tags.is_empty() {
                    { format!(" ({})", feat.tags) }
                }
            </h1>
            <p class={classes!("py-1")}><b>{ "Source " }</b>{ feat.source.clone() }</p>
            <p class={classes!("py-1")}>{ feat.description.to_revealables(onclick.clone(), hidden_handle.clone()) }</p>
            if !feat.prerequisites.0.is_empty() {
                <p class={classes!("py-1")}><b>{ "Prerequisites: " }</b>{ feat.prerequisites.to_revealables(onclick.clone(), hidden_handle.clone()) }</p>
            }
            <p class={classes!("py-1")}><b>{ "Benefit: " }</b>{ feat.benefit.to_revealables(onclick.clone(), hidden_handle.clone()) }</p>
            if !feat.normal.0.is_empty() {
                <p class={classes!("py-1")}><b>{ "Normal: " }</b>{ feat.normal.to_revealables(onclick.clone(), hidden_handle.clone()) }</p>
            }
            if !feat.special.0.is_empty() {
                <p class={classes!("py-1")}><b>{ "Special: " }</b>{ feat.special.to_revealables(onclick.clone(), hidden_handle.clone()) }</p>
            }
            if !feat.note.0.is_empty() {
                <p class={classes!("py-1")}><b>{ "Note: " }</b>{ feat.note.to_revealables(onclick.clone(), hidden_handle.clone()) }</p>
            }
            if !feat.goal.0.is_empty() {
                <p class={classes!("py-1")}><b>{ "Goal: " }</b>{ feat.goal.to_revealables(onclick.clone(), hidden_handle.clone()) }</p>
            }
            if !feat.completion_benefit.0.is_empty() {
                <p class={classes!("py-1")}><b>{ "Completion Benefit: " }</b>{ feat.completion_benefit.to_revealables(onclick.clone(), hidden_handle.clone()) }</p>
            }
            if !feat.suggested_traits.0.is_empty() {
                <p class={classes!("py-1")}><b>{ "Suggested Traits: " }</b>{ feat.suggested_traits.to_revealables(onclick, hidden_handle) }</p>
            }
        </>
    }
}
