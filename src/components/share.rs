use gloo::{timers::callback::Timeout, utils::window};
use wasm_bindgen::closure::Closure;
use wasm_bindgen_futures::JsFuture;
use yew::{platform::spawn_local, prelude::*};

use crate::redacted::{RedactedFeat, Token};

#[derive(PartialEq, Properties)]
pub struct ShareProps {
    pub day: i64,
    pub feat: UseStateHandle<RedactedFeat>,
    pub guess_counter: UseStateHandle<u32>,
}

#[function_component]
pub fn Share(props: &ShareProps) -> Html {
    let ShareProps {
        day,
        feat,
        guess_counter,
    } = props;

    let copied = use_state_eq(|| false);

    let onclick = {
        let day = *day;
        let feat = feat.clone();
        let guess_counter = guess_counter.clone();
        let copied = copied.clone();

        Callback::from(move |_| {
            let guess_counter = guess_counter.clone();
            let copied = copied.clone();

            let clipboard = window().navigator().clipboard();
            let meme_boxes = feat
                .answer
                .0
                .iter()
                .cloned()
                .filter_map(|w| match w {
                    Token::Guessable(w) => {
                        if w.revealed {
                            Some('🟦')
                        } else {
                            Some('⬜')
                        }
                    }
                    Token::Revealed(_) => Some('🟦'),
                    Token::Space => None,
                })
                .collect::<String>();
            let share_text = format!(
                "[Pathfindle](https://tassad.gitlab.io/pathfindle) {}\n\n{}    {}",
                day + 1,
                meme_boxes,
                *guess_counter
            );

            let cb = Closure::new(move |_| {
                copied.set(true);
                {
                    let copied = copied.clone();
                    Timeout::new(1000, move || {
                        copied.set(false);
                    })
                    .forget();
                }
            });

            spawn_local(async move {
                let _ = JsFuture::from(clipboard.write_text(&share_text).then(&cb)).await;
            });
        })
    };

    html! {
        <>
            <button {onclick} class={classes!("cursor-pointer", "mb-4", "p-1", "border", "border-black")}>
                { "Share Result" }
            </button>
            <span hidden={!*copied}>{ " Copied to clipboard" }</span>
        </>
    }
}
