use std::{borrow::Cow, fmt::Display};

use chumsky::prelude::*;
use yew::{
    classes, html,
    prelude::{Callback, Html, MouseEvent, UseStateHandle},
};

use pf_feat::PFFeat;

const STOP_WORDS: [&str; 98] = [
    "a",
    "aboard",
    "about",
    "above",
    "across",
    "after",
    "against",
    "along",
    "amid",
    "among",
    "an",
    "and",
    "are",
    "around",
    "as",
    "at",
    "because",
    "be",
    "been",
    "being",
    "before",
    "behind",
    "below",
    "beneath",
    "beside",
    "between",
    "beyond",
    "but",
    "by",
    "concerning",
    "considering",
    "despite",
    "down",
    "during",
    "except",
    "'d",
    "had",
    "has",
    "'ve",
    "have",
    "following",
    "for",
    "from",
    "if",
    "in",
    "inside",
    "into",
    "is",
    "it",
    "like",
    "minus",
    "near",
    "next",
    "of",
    "off",
    "on",
    "onto",
    "opposite",
    "or",
    "out",
    "outside",
    "over",
    "past",
    "per",
    "plus",
    "regarding",
    "round",
    "since",
    "'s",
    "than",
    "that",
    "the",
    "there",
    "they",
    "this",
    "through",
    "till",
    "to",
    "toward",
    "under",
    "underneath",
    "unlike",
    "until",
    "up",
    "upon",
    "use",
    "used",
    "uses",
    "using",
    "versus",
    "via",
    "was",
    "were",
    "which",
    "'ll",
    "with",
    "within",
    "without",
];

#[derive(PartialEq, Debug)]
pub struct RedactedFeat {
    pub answer: RedactedWords,
    pub tags: String,
    pub source: String,
    pub description: RedactedWords,
    pub prerequisites: RedactedWords,
    pub benefit: RedactedWords,
    pub normal: RedactedWords,
    pub special: RedactedWords,
    pub note: RedactedWords,
    pub goal: RedactedWords,
    pub completion_benefit: RedactedWords,
    pub suggested_traits: RedactedWords,
}

#[derive(PartialEq, Clone, Debug)]
pub struct RedactedWord {
    pub word: String,
    pub revealed: bool,
}

#[derive(PartialEq, Clone, Debug)]
pub enum Token {
    Guessable(RedactedWord),
    Revealed(String),
    Space,
}

#[derive(PartialEq, Debug)]
pub struct RedactedWords(pub Vec<Token>);

impl RedactedFeat {
    pub fn new(feat: &PFFeat) -> Self {
        Self {
            answer: parser().parse(feat.name.as_ref()).unwrap().into(),
            tags: feat.tags.as_ref().unwrap_or(&Cow::Borrowed("")).to_string(),
            source: feat.source.to_string(),
            description: parser().parse(feat.description.as_ref()).unwrap().into(),
            prerequisites: (&feat.prerequisites).into(),
            benefit: parser().parse(feat.benefit.as_ref()).unwrap().into(),
            normal: (&feat.normal).into(),
            special: (&feat.special).into(),
            note: (&feat.note).into(),
            goal: (&feat.goal).into(),
            completion_benefit: (&feat.completion_benefit).into(),
            suggested_traits: (&feat.suggested_traits).into(),
        }
    }
}

impl RedactedWords {
    pub fn do_guess(&self, guess: &str) -> (Self, u32) {
        let mut count: u32 = 0;

        let words = self
            .0
            .iter()
            .cloned()
            .map(|mut w| {
                if let Token::Guessable(w) = &mut w {
                    if w.word.to_lowercase() == guess {
                        w.revealed = true;
                        count += 1;
                    }
                }

                w
            })
            .collect();

        (Self(words), count)
    }

    pub fn reveal_all(&self) -> Self {
        Self(
            self.0
                .iter()
                .cloned()
                .map(|mut w| {
                    if let Token::Guessable(w) = &mut w {
                        w.revealed = true;
                    };
                    w
                })
                .collect(),
        )
    }

    pub fn to_revealables(
        &self,
        onclick: Callback<MouseEvent>,
        hidden_state: UseStateHandle<bool>,
    ) -> Html {
        html! {
            <>
                {for self.0.iter().map(|token| {
                    match token {
                        Token::Guessable(w) => {
                            if w.revealed {
                                html! {
                                    <>{w.word.to_string()}</>
                                }
                            } else {
                                html! {
                                    <span onclick={onclick.clone()} class={classes!("relative", "cursor-pointer", "select-none")}>
                                        {str::repeat("█", w.word.len())}
                                        <span hidden={*hidden_state} class={classes!("absolute", "left-0", "top-0", "text-black")}>{w.word.len()}</span>
                                    </span>
                                }
                            }
                        }
                        Token::Revealed(w) => html! { <>{w}</> },
                        Token::Space => html! { <>{" "}</> },
                    }
                })}
            </>
        }
    }
}

impl From<&Option<Cow<'_, str>>> for RedactedWords {
    fn from(value: &Option<Cow<'_, str>>) -> Self {
        Self(
            parser()
                .parse(value.as_ref().unwrap_or(&Cow::Borrowed("")).as_ref())
                .unwrap(),
        )
    }
}

impl From<Vec<Token>> for RedactedWords {
    fn from(value: Vec<Token>) -> Self {
        Self(value)
    }
}

impl Display for RedactedWords {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for word in &self.0 {
            match word {
                Token::Guessable(w) => {
                    if w.revealed {
                        write!(f, "{}", w.word)?;
                    }
                }
                Token::Revealed(w) => write!(f, "{}", w)?,
                Token::Space => write!(f, " ")?,
            }
        }

        Ok(())
    }
}

fn parser() -> impl Parser<char, Vec<Token>, Error = Simple<char>> {
    let space = just(' ').repeated().at_least(1).map(|_| Token::Space);

    let superscript = filter(char::is_ascii_uppercase)
        .repeated()
        .at_least(2)
        .collect()
        .map(Token::Revealed);

    let lowercase_or_digits =
        filter(|c: &char| c.is_ascii_lowercase() || c.is_ascii_digit()).repeated();

    let word = filter(char::is_ascii_alphanumeric)
        .chain(lowercase_or_digits)
        .collect()
        .map(|word: String| {
            if STOP_WORDS.contains(&word.to_lowercase().as_str()) {
                Token::Revealed(word)
            } else {
                Token::Guessable(RedactedWord {
                    word,
                    revealed: false,
                })
            }
        });

    let punctuation = filter(|c: &char| !c.is_alphanumeric())
        .repeated()
        .at_least(1)
        .collect()
        .map(Token::Revealed);

    choice((space, superscript, word, punctuation))
        .repeated()
        .collect()
}
