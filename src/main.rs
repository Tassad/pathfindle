use indexmap::IndexMap;
use time::OffsetDateTime;
use yew::prelude::*;

use crate::components::{
    feat_display::FeatDisplay, footer::Footer, guess_counter::GuessCounter, input::Input, log::Log,
    share::Share,
};
use crate::feat::PF_FEATS;
use crate::redacted::{RedactedFeat, Token};

mod components;
mod feat;
mod redacted;

// 2023-11-29 00:00:00 UTC
const DAY_1: Result<OffsetDateTime, time::error::ComponentRange> =
    OffsetDateTime::from_unix_timestamp(1_701_216_000);

#[function_component]
fn App() -> Html {
    let day = (OffsetDateTime::now_utc() - DAY_1.unwrap()).whole_days();
    let feat = use_state(|| RedactedFeat::new(&PF_FEATS[day as usize % PF_FEATS.len()]));
    let guess_counter = use_state(|| 0);
    let victory = use_state(|| false);
    let guess_log = use_state(IndexMap::new);
    let last_guess = use_state_eq(|| None);

    let on_guess = {
        let feat = feat.clone();
        let guess_counter = guess_counter.clone();
        let victory = victory.clone();
        let guess_log = guess_log.clone();
        let last_guess = last_guess.clone();

        Callback::from(move |guess: AttrValue| {
            if *victory {
                return;
            }

            if guess_log.contains_key(&guess) {
                last_guess.set(Some(guess.to_owned()));
                return;
            } else {
                last_guess.set(None);
            }

            guess_counter.set(*guess_counter + 1);
            let mut new_log = (*guess_log).clone();

            let revealed_answer = feat.answer.reveal_all();
            let answer = if revealed_answer.to_string().to_lowercase().trim() == guess {
                (revealed_answer, 0)
            } else {
                feat.answer.do_guess(&guess)
            };

            if answer.0 .0.iter().all(|w| match w {
                Token::Guessable(w) => w.revealed,
                _ => true,
            }) {
                victory.set(true);

                new_log.insert(guess.to_owned(), "*".into());
                guess_log.set(new_log);

                feat.set(RedactedFeat {
                    answer: answer.0,
                    tags: feat.tags.clone(),
                    source: feat.source.clone(),
                    description: feat.description.reveal_all(),
                    prerequisites: feat.prerequisites.reveal_all(),
                    benefit: feat.benefit.reveal_all(),
                    normal: feat.normal.reveal_all(),
                    special: feat.special.reveal_all(),
                    note: feat.note.reveal_all(),
                    goal: feat.goal.reveal_all(),
                    completion_benefit: feat.completion_benefit.reveal_all(),
                    suggested_traits: feat.suggested_traits.reveal_all(),
                });

                return;
            }

            let description = feat.description.do_guess(&guess);
            let prerequisites = feat.prerequisites.do_guess(&guess);
            let benefit = feat.benefit.do_guess(&guess);
            let normal = feat.normal.do_guess(&guess);
            let special = feat.special.do_guess(&guess);
            let note = feat.note.do_guess(&guess);
            let goal = feat.goal.do_guess(&guess);
            let completion_benefit = feat.completion_benefit.do_guess(&guess);
            let suggested_traits = feat.suggested_traits.do_guess(&guess);

            new_log.insert(
                guess.to_owned(),
                [
                    &answer,
                    &description,
                    &prerequisites,
                    &benefit,
                    &normal,
                    &special,
                    &note,
                    &goal,
                    &completion_benefit,
                    &suggested_traits,
                ]
                .iter()
                .fold(0, |acc, w| acc + w.1)
                .to_string()
                .into(),
            );
            guess_log.set(new_log);

            feat.set(RedactedFeat {
                answer: answer.0,
                tags: feat.tags.clone(),
                source: feat.source.clone(),
                description: description.0,
                prerequisites: prerequisites.0,
                benefit: benefit.0,
                normal: normal.0,
                special: special.0,
                note: note.0,
                goal: goal.0,
                completion_benefit: completion_benefit.0,
                suggested_traits: suggested_traits.0,
            });
        })
    };

    html! {
        <div class={classes!("font-mono", "min-h-screen", "flex", "flex-col", "bg-gray-800", "text-white", "selection:text-black", "selection:bg-blue-300")}>
            <main class={classes!("flex", "flex-auto", "max-lg:flex-col", "lg:flex-row")}>
                <div class={classes!("basis-3/4", "flex-auto", "min-w-min", "px-2", "pt-2")}>
                    <Share {day} feat={feat.clone()} guess_counter={guess_counter.clone()} />
                    if *victory {
                        <h1 class={classes!("text-3xl")}>{ format!("You found the secret feat in {} guesses!", guess_counter.to_string()) }</h1>
                    }
                    <FeatDisplay {feat} />
                </div>
                <div class={classes!("basis-1/4", "flex", "flex-col", "flex-initial", "bg-gray-900", "pr-2", "max-h-full", "overflow-auto", "max-lg:text-5xl")}>
                    <GuessCounter {guess_counter} />
                    <Input {on_guess} />
                    <Log {guess_log} {last_guess} />
                </div>
            </main>
            <Footer />
        </div>
    }
}

fn main() {
    console_error_panic_hook::set_once();

    wasm_logger::init(wasm_logger::Config::default());

    yew::Renderer::<App>::new().render();
}
