use pf_feat::PFFeat;

use feat_parser::parse_feats;

parse_feats!("pathfinder-feats.ron");
