use quote::quote;
use rand::prelude::{SeedableRng, SliceRandom};
use rand_chacha::ChaCha8Rng;
use syn::{parse_macro_input, LitStr};

use pf_feat::PFFeat;

#[proc_macro]
pub fn parse_feats(input: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input = parse_macro_input!(input as LitStr);

    let mut rng = ChaCha8Rng::seed_from_u64(69420);

    let mut feats: Vec<PFFeat> =
        ron::from_str(&std::fs::read_to_string(input.value()).unwrap()).unwrap();
    let feats_len = feats.len();
    feats.shuffle(&mut rng);

    let out = quote!(
        pub const PF_FEATS: [PFFeat; #feats_len] = [
            #(#feats),*
        ];
    );

    out.into()
}
